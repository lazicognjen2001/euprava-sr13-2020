package com.ognjenlazic.eUprava.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;


import com.ognjenlazic.eUprava.bean.SecondConfiguration.ApplicationMemory;
import com.ognjenlazic.eUprava.model.User;
import com.ognjenlazic.eUprava.model.UserType;
import com.ognjenlazic.eUprava.model.Users;
import com.ognjenlazic.eUprava.model.VaccineDose;
import com.ognjenlazic.eUprava.model.VaccineDoses;
import com.ognjenlazic.eUprava.model.Vaccines;

@Controller
@RequestMapping(value = "/users")
public class UsersController {
	
public static final String USER_KEY = "user";
public static final String USERS_KEY = "users";
public static final String VACCINE_DOSES_KEY = "vaccineDoses";
public static final String VACCINES_KEY = "vaccines";


	@Autowired
	private ApplicationMemory appMemory;
	
	@Autowired
	private ApplicationContext applicationContext;
	
	@Autowired
	private ServletContext servletContext;
	
	private  String bURL; 
	
	@PostConstruct
	public void init() {	
		bURL = servletContext.getContextPath()+"/";
		appMemory = applicationContext.getBean(ApplicationMemory.class);
		
		Users users = new Users();
		VaccineDoses vaccineDoses = new VaccineDoses();
		Vaccines vaccines = new Vaccines();
		
		for (User user: users.findAll()) {
			for(VaccineDose dose : vaccineDoses.findAll()) {
				if(dose.getUserId() == user.getId()) {
					user.getVaccineDoses().add(dose);
					dose.setUser(user);
				}
			}
		}
		
		appMemory.put(UsersController.VACCINE_DOSES_KEY, vaccineDoses);
 		appMemory.put(UsersController.USERS_KEY, users);
 		appMemory.put(UsersController.VACCINES_KEY, vaccines);
	}

	@GetMapping(value = "/login")
	public void getLogin(@RequestParam(required = false) Long jmbg, @RequestParam(required = false) String password,
			HttpSession session, HttpServletResponse response) throws IOException {
		postLogin(jmbg, password, session, response);
		System.out.println("DOŠLI SMO U KLASU USER getLogin");

	}

	@SuppressWarnings("unchecked")
	@PostMapping(value = "/login")
	@ResponseBody
	public void postLogin(@RequestParam(required = false) Long jmbg, @RequestParam(required = false) String password,
			HttpSession session, HttpServletResponse response) throws IOException {
		System.out.println("DOŠLI SMO U KONTROLER USER postLogin");


		Users users = (Users) appMemory.get(UsersController.USERS_KEY);
		if (users != null) {
			if (users.users.size() == 0) {
				users = new Users();
				appMemory.put(UsersController.USERS_KEY, users);
			}
		}
		
		
		User user = users.findOneByJmbg(jmbg);
		String error = "";
		if (user == null)
			error = "neispravno korisničko ime<br/>";
		else if (!user.getPassword().equals(password))
			error = "neispravna korisnika šifra<br/>";
			

		if (!error.equals("")) {
			response.setContentType("text/html; charset=UTF-8");
			PrintWriter out;
			out = response.getWriter();

			StringBuilder retVal = new StringBuilder();
			retVal.append("<!DOCTYPE html>\r\n" + "<html>\r\n" + "<head>\r\n" + "	<meta charset=\"UTF-8\">\r\n"
					+ "	<base href=\"/eUprava/\">	\r\n" + "	<title>Prijava korisnika</title>\r\n"
					+ "	<link rel=\"stylesheet\" type=\"text/css\" href=\"css/StiloviForma.css\"/>\r\n"
					+ "	<link rel=\"stylesheet\" type=\"text/css\" href=\"css/StiloviHorizontalniMeni.css\"/>\r\n"
					+ "</head>\r\n" + "<body>\r\n" + "	<ul>\r\n");
			if (!error.equals(""))
				retVal.append("	<div>" + error + "</div>\r\n");
			retVal.append("	<form method=\"post\" action=\"users/login\">\r\n" + "		<table>\r\n"
					+ "			<caption>Prijava korisnika na sistem</caption>\r\n"
					+ "			<tr><th>JMBG:</th><td><input type=\"text\" value=\"\" name=\"jmbg\" pattern=\"[0-9]{1,14}\" required/></td></tr>\r\n"
					+ "			<tr><th>Šifra:</th><td><input type=\"password\" value=\"\" name=\"password\" required/></td></tr>\r\n"
					+ "			<tr><th></th><td><input type=\"submit\" value=\"Prijavi se\" /></td>\r\n"
					+ "		</table>\r\n" + "	</form>\r\n" + "	<br/>\r\n" + "</body>\r\n" + "</html>");

			out.write(retVal.toString());
			return;
		}

		if (session.getAttribute(USER_KEY) != null)
			error = "korisnik je već prijavljen na sistem morate se prethodno odjaviti<br/>";

		if (!error.equals("")) {
			response.setContentType("text/html; charset=UTF-8");
			PrintWriter out;
			out = response.getWriter();

			StringBuilder retVal = new StringBuilder();
			retVal.append("<!DOCTYPE html>\r\n" + "<html>\r\n" + "<head>\r\n" + "	<meta charset=\"UTF-8\">\r\n"
					+ "	<base href=\"/eUprava/\">	\r\n" + "	<title>Prijava korisnika</title>\r\n"
					+ "	<link rel=\"stylesheet\" type=\"text/css\" href=\"css/StiloviForma.css\"/>\r\n"
					+ "	<link rel=\"stylesheet\" type=\"text/css\" href=\"css/StiloviHorizontalniMeni.css\"/>\r\n"
					+ "</head>\r\n" + "<body>\r\n" + "	<ul>\r\n");
			if (!error.equals(""))
				retVal.append("	<div>" + error + "</div>\r\n");
			retVal.append("	<a href=\"index.html\">Povratak</a>\r\n" + "	<br/>\r\n" + "</body>\r\n" + "</html>");

			out.write(retVal.toString());
			return;
		}

		appMemory.put(UsersController.USER_KEY, user); 
			System.out.println("Tip korisnika je: " + user.getUserType());
		if (user.getUserType() == UserType.medic) {
			System.out.println("PRIJAVLJEN JE PACIJENT");
			response.sendRedirect(bURL + "medic");
		} else if (user.getUserType() == UserType.patient)  {
			System.out.println("PRIJAVLJEN JE MEDICINAR");
			response.sendRedirect(bURL + "patient");
		} else  {
			System.out.println("NIJE PRIJAVLJEN NITI JEDAN");
		}
	}
	
	

}
