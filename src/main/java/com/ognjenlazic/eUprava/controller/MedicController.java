package com.ognjenlazic.eUprava.controller;

import java.io.IOException;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;


import com.ognjenlazic.eUprava.bean.SecondConfiguration.ApplicationMemory;
import com.ognjenlazic.eUprava.model.User;
import com.ognjenlazic.eUprava.model.UserType;
import com.ognjenlazic.eUprava.model.Users;
import com.ognjenlazic.eUprava.model.VaccineDose;
import com.ognjenlazic.eUprava.model.VaccineDoses;

@Controller
@RequestMapping(value="/medic")
public class MedicController implements ApplicationContextAware {
	
public static final String MEDIC_KEY = "medic";
	
	@Autowired
	private ServletContext servletContext;
	private  String bURL; 
	
	@Autowired
	private ApplicationContext applicationContext;
	
	@Autowired
	private ApplicationMemory appMemory;

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
	    this.applicationContext = applicationContext;    
	}
	
	@PostConstruct
	public void init() {	
		bURL = servletContext.getContextPath()+"/";
		appMemory = applicationContext.getBean(ApplicationMemory.class);
		System.out.println("Medic kontroler je inicijalizovan");


	}
	
	@GetMapping
	@ResponseBody
	public String index() {	
		
				
		
		VaccineDoses vaccineDoses = (VaccineDoses) appMemory.get(UsersController.VACCINE_DOSES_KEY);
		Users users = (Users) appMemory.get(UsersController.USERS_KEY);

			
		StringBuilder retVal = new StringBuilder();
		retVal.append(
				"<!DOCTYPE html>\r\n" + 
				"<html>\r\n" + 
				"<head>\r\n" + 
				"	<meta charset=\"UTF-8\">\r\n" + 
	    		"	<base href=\""+bURL+"\">" + 
				"	<title>Medicinski radnik</title>\r\n" + 
				"	<link rel=\"stylesheet\" type=\"text/css\" href=\"css/StiloviTabela.css\"/>\r\n" + 
				"	<link rel=\"stylesheet\" type=\"text/css\" href=\"css/StiloviHorizontalniMeni.css\"/>\r\n"+
				"</head>\r\n" + 
				"<body> "+
				"	<ul>\r\n" + 
				"		<li><a href=\"medic\">Početna</a></li>\r\n" + 
				"		<li><a href=\"medic\\allUsers\">Svi korisnici</a></li>\r\n" + 
				"		<li><a href=\"\">Odjava</a></li>\r\n" + 
				
				"	</ul>\r\n" + 
				"		<table>\r\n" + 
				"			<caption>Rezervisani termini</caption>\r\n" + 
				"			<tr>\r\n" + 
				"				<th>ID</th>\r\n" + 
				"				<th>JMBG</th>\r\n" + 
				"				<th>Ime</th>\r\n" + 
				"				<th>Prezime</th>\r\n" +
				"				<th>Vakcina</th>\r\n" +
				"				<th>Doza</th>\r\n" +
				"				<th></th>\r\n" +
				"				<th></th>\r\n" +
				"			</tr>\r\n");
		
		List<VaccineDose> vaccineDosesList = vaccineDoses.findAll();
		for (int i=0; i < vaccineDosesList.size(); i++) {
			if (!vaccineDosesList.get(i).isVaccinated()) {
				int userId = vaccineDosesList.get(i).getUserId();
				retVal.append(
					"			<tr>\r\n" + 
					"				<td>" +vaccineDosesList.get(i).getDoseId() +"</td>\r\n" +
					"				<td>"+ users.findOne(userId).getJmbg() +"</td>\r\n" +
					"				<td>"+ users.findOne(userId).getName() +"</td>\r\n" +
					"				<td>"+ users.findOne(userId).getSurname() +"</td>\r\n" +
					"				<td>"+ vaccineDosesList.get(i).getVaccineId() +"</td>\r\n" +
					"				<td class=\"broj\">"+ vaccineDosesList.get(i).getNumberOfDose() +"</td>\r\n" +
					"				<td>" + 
					"					<form method=\"post\" action=\"medic/vaccinationCard?id="+vaccineDosesList.get(i).getUserId()+"\">\r\n" + 
					"						<input type=\"hidden\" name=\"id\" value=\""+vaccineDosesList.get(i).getUserId()+"\">\r\n" + 
					"						<input type=\"submit\" value=\"Pogledaj karton\"></td>\r\n" + 
					"					</form>\r\n" +
					"				</td>" +
					"				<td>" + 
					"					<form method=\"post\" action=\"medic/vaccinate\">\r\n" + 
					"						<input type=\"hidden\" name=\"id\" value=\""+vaccineDosesList.get(i).getDoseId()+"\">\r\n" + 
					"						<input type=\"submit\" value=\"Vakciniši\"></td>\r\n" + 
					"					</form>\r\n" +
					"				</td>" +
					"			</tr>\r\n");
			}
		}
		
		retVal.append(
				"		</table>\r\n");

		retVal.append(
				"</body>\r\n"+
				"</html>\r\n");		
		return retVal.toString();
		
	}
	
	@PostMapping(value="/vaccinate")
	public void delete(@RequestParam Integer id, HttpServletResponse response) throws IOException {

		VaccineDoses vaccineDoses = (VaccineDoses) appMemory.get(UsersController.VACCINE_DOSES_KEY);
		
		VaccineDose vaccinated = vaccineDoses.vaccinate(id);
		vaccineDoses.saveInFile();
		response.sendRedirect(bURL+"medic");
	}
	
	@PostMapping(value="/vaccinationCard")
	@ResponseBody
	public String details(@RequestParam Integer id) {	

		Users users = (Users) appMemory.get(UsersController.USERS_KEY);
		VaccineDoses vaccineDoses = (VaccineDoses) appMemory.get(UsersController.VACCINE_DOSES_KEY);
		User user = users.findOne(id);
		
		StringBuilder retVal = new StringBuilder();
		retVal.append(
				"<!DOCTYPE html>\r\n" + 
				"<html>\r\n" + 
				"<head>\r\n" + 
				"	<meta charset=\"UTF-8\">\r\n" + 
	    		"	<base href=\""+bURL+"\">" + 
				"	<title>Vakcinalni karton</title>\r\n" + 
				"	<link rel=\"stylesheet\" type=\"text/css\" href=\"css/StiloviTabela.css\"/>\r\n" + 
				"	<link rel=\"stylesheet\" type=\"text/css\" href=\"css/StiloviHorizontalniMeni.css\"/>\r\n"+
				"</head>\r\n" + 
				"<body> "+
				"	<ul>\r\n" + 
				"		<li><a href=\"medic\">Početna</a></li>\r\n" + 
				"		<li><a href=\"medic\\allUsers\">Svi korisnici</a></li>\r\n" + 
				"		<li><a href=\"\">Odjava</a></li>\r\n" + 
				"	</ul>\r\n" + 
				"		<table>\r\n" + 
				"			<caption>Vakcinalni karton - Pacijent: "+ user.getJmbg() + "</caption>\r\n" + 
				"			<tr>\r\n" + 
				"				<th>ID</th>\r\n" + 
				"				<th>JMBG</th>\r\n" + 
				"				<th>Ime</th>\r\n" + 
				"				<th>Prezime</th>\r\n" +
				"				<th>Vakcina</th>\r\n" +
				"				<th>Doza</th>\r\n" +
				"				<th>Datum prijave</th>\r\n" +
				"				<th>Datum vakcinacije</th>\r\n" +
				"			</tr>\r\n");
		
		List<VaccineDose> vaccineDosesList = vaccineDoses.findAll();
		for (int i=0; i < vaccineDosesList.size(); i++) {
			if (user.getId() == vaccineDosesList.get(i).getUserId()) {
				retVal.append(
					"			<tr>\r\n" + 
					"				<td>" +vaccineDosesList.get(i).getDoseId() +"</td>\r\n" +
					"				<td>"+ user.getJmbg() +"</td>\r\n" +
					"				<td>"+ user.getName() +"</td>\r\n" +
					"				<td>"+user.getSurname() +"</td>\r\n" +
					"				<td>"+ vaccineDosesList.get(i).getVaccineId() +"</td>\r\n" +
					"				<td class=\"broj\">Doza "+ vaccineDosesList.get(i).getNumberOfDose() +"</td>\r\n" +
					"				<td>"+ vaccineDosesList.get(i).getDateOfReservation() +"</td>\r\n" +
					"				<td>"+ vaccineDosesList.get(i).getDateOfDose() +"</td>\r\n" +
					"				</td>" +
					"			</tr>\r\n");
			}
		}
		
		retVal.append(
				"		</table>\r\n");
		retVal.append(
				"</body>\r\n"+
				"</html>\r\n");		
		return retVal.toString();
	}
	
	
	@GetMapping(value="/allUsers")
	@ResponseBody
	public String allUsers() {	

		Users users = (Users) appMemory.get(UsersController.USERS_KEY);
		
		StringBuilder retVal = new StringBuilder();
		retVal.append(
				"<!DOCTYPE html>\r\n" + 
				"<html>\r\n" + 
				"<head>\r\n" + 
				"	<meta charset=\"UTF-8\">\r\n" + 
	    		"	<base href=\""+bURL+"\">" + 
				"	<title>Vakcinalni karton</title>\r\n" + 
				"	<link rel=\"stylesheet\" type=\"text/css\" href=\"css/StiloviTabela.css\"/>\r\n" + 
				"	<link rel=\"stylesheet\" type=\"text/css\" href=\"css/StiloviHorizontalniMeni.css\"/>\r\n"+
				"</head>\r\n" + 
				"<body> "+
				"	<ul>\r\n" + 
				"		<li><a href=\"medic\">Početna</a></li>\r\n" + 
				"		<li><a href=\"medic\\allUsers\">Svi korisnici</a></li>\r\n" + 
				"		<li><a href=\"\">Odjava</a></li>\r\n" + 
				"	</ul>\r\n" + 
				"		<table>\r\n" + 
				"			<caption>Svi korisnici</caption>\r\n" + 
				"			<tr>\r\n" + 
				"				<th>ID</th>\r\n" + 
				"				<th>JMBG</th>\r\n" + 
				"				<th>Ime</th>\r\n" + 
				"				<th>Prezime</th>\r\n" +
				"				<th></th>\r\n" +
				
				"			</tr>\r\n");
		
		List<User> allUsers = users.findAll();
		for (int i=0; i < allUsers.size(); i++) {
			User user = allUsers.get(i);
			if(user.getUserType() != UserType.medic) {
				retVal.append(
					"			<tr>\r\n" + 
					"				<td>" + user.getId() +"</td>\r\n" +
					"				<td>"+ user.getJmbg() +"</td>\r\n" +
					"				<td>"+ user.getName() +"</td>\r\n" +
					"				<td>"+ user.getSurname() +"</td>\r\n" +
					"<td>" + 
					"					<form method=\"post\" action=\"medic/vaccinationCard?id="+user.getId()+"\">\r\n" + 
					"						<input type=\"hidden\" name=\"id\" value=\""+user.getId()+"\">\r\n" + 
					"						<input type=\"submit\" value=\"Pogledaj karton\"></td>\r\n" + 
					"					</form>\r\n" +
					"				</td>" +
					"				</td>" +
					"			</tr>\r\n");
			
				}
			}
		
		retVal.append(
				"		</table>\r\n");

		retVal.append(
				"</body>\r\n"+
				"</html>\r\n");		
		return retVal.toString();
	}

}
