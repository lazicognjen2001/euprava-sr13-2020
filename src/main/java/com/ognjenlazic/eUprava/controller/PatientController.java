package com.ognjenlazic.eUprava.controller;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ognjenlazic.eUprava.bean.SecondConfiguration.ApplicationMemory;
import com.ognjenlazic.eUprava.model.User;
import com.ognjenlazic.eUprava.model.Vaccine;
import com.ognjenlazic.eUprava.model.VaccineDose;
import com.ognjenlazic.eUprava.model.VaccineDoses;
import com.ognjenlazic.eUprava.model.Vaccines;

@Controller
@RequestMapping(value="/patient")
public class PatientController implements ApplicationContextAware {
public static final String MEDIC_KEY = "medic";
	
	@Autowired
	private ServletContext servletContext;
	private  String bURL; 
	
	@Autowired
	private ApplicationContext applicationContext;
	
	@Autowired
	private ApplicationMemory appMemory;

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
	    this.applicationContext = applicationContext;    
	}
	
	@PostConstruct
	public void init() {	
		bURL = servletContext.getContextPath()+"/";
		appMemory = applicationContext.getBean(ApplicationMemory.class);
	}
	

	@GetMapping
	@ResponseBody
	public String index() {	
		
		VaccineDoses vaccineDoses = (VaccineDoses) appMemory.get(UsersController.VACCINE_DOSES_KEY);
		User user = (User) appMemory.get(UsersController.USER_KEY);
			
		StringBuilder retVal = new StringBuilder();
		retVal.append(
				"<!DOCTYPE html>\r\n" + 
				"<html>\r\n" + 
				"<head>\r\n" + 
				"	<meta charset=\"UTF-8\">\r\n" + 
	    		"	<base href=\""+bURL+"\">" + 
				"	<title>Pacijent</title>\r\n" + 
				"	<link rel=\"stylesheet\" type=\"text/css\" href=\"css/StiloviTabela.css\"/>\r\n" + 
				"	<link rel=\"stylesheet\" type=\"text/css\" href=\"css/StiloviHorizontalniMeni.css\"/>\r\n"+
				"</head>\r\n" + 
				"<body> "+
				"	<ul>\r\n" + 
				"		<li><a href=\"patient\">Početna</a></li>\r\n" + 
				"		<li><a href=\"patient\\newDose\">Zakaži novu dozu</a></li>\r\n" + 
				"		<li><a href=\"\">Odjava</a></li>\r\n" + 
				"	</ul>\r\n" + 
				"		<table>\r\n" + 
				"			<caption>Vaše doze "+ user.getName() + " " + user.getSurname() + "</caption>\r\n" + 
				"			<tr>\r\n" + 
				"				<th>ID Doze</th>\r\n" + 
				"				<th>JMBG</th>\r\n" + 
				"				<th>Ime</th>\r\n" + 
				"				<th>Prezime</th>\r\n" +
				"				<th>Vakcina</th>\r\n" +
				"				<th>Doza</th>\r\n" +
				"				<th>Datum rezervacije</th>\r\n" +
				"				<th>Datum vakcinacije</th>\r\n" +
				"				<th></th>\r\n" +
				"			</tr>\r\n");
		
		for (VaccineDose dose :  vaccineDoses.vaccineDoses.values()) {
			if (dose.getUserId() == user.getId()) {
				retVal.append(
					"			<tr>\r\n" + 
					"				<td>" + dose.getDoseId() +"</td>\r\n" +
					"				<td>"+ user.getJmbg() +"</td>\r\n" +
					"				<td>"+ user.getName() +"</td>\r\n" +
					"				<td>"+ user.getSurname() +"</td>\r\n" +
					"				<td>"+ dose.getVaccineId() +"</td>\r\n" +
					"				<td>"+ dose.getNumberOfDose() +"</td>\r\n" +
					"				<td>"+ dose.getDateOfReservation() +"</td>\r\n" +
					"				<td>"+ dose.getDateOfDose() +"</td>\r\n" );
				
				if (!dose.isVaccinated()) {
					retVal.append("				<td>" + 
							"					<form method=\"post\" action=\"patient/doseEdit?id="+dose.getDoseId()+"\">\r\n" + 
							"						<input type=\"hidden\" name=\"doseId\" value=\""+dose.getDoseId()+"\">\r\n" + 
							"						<input type=\"submit\" value=\"Izmjeni\"></td>\r\n" + 
							"					</form>\r\n" +
							"				</td>");
				} else {
					retVal.append("				<td>" + 
							"				</td>");
				}
				
					retVal.append("</tr>\r\n");
			}
		}
		
		retVal.append(
				"		</table>\r\n");
		
		retVal.append(
				"</body>\r\n"+
				"</html>\r\n");		
		return retVal.toString();
		
	}
	
		@GetMapping(value="/newDose")
		@ResponseBody
		public String create() {
			Vaccines vaccines = (Vaccines) appMemory.get(UsersController.VACCINES_KEY);
			User user = (User) appMemory.get(UsersController.USER_KEY);

			int doseNumber = user.getVaccineDoses().size() + 1;
			StringBuilder retVal = new StringBuilder();
			retVal.append(
					"<!DOCTYPE html>\r\n" + 
					"<html>\r\n" + 
					"<head>\r\n" + 
					"	<meta charset=\"UTF-8\">\r\n" + 
		    		"	<base href=\""+bURL+"\">" + 
					"	<title>Dodaj clansku kartu</title>\r\n" + 
					"	<link rel=\"stylesheet\" type=\"text/css\" href=\"css/StiloviForma.css\"/>\r\n" + 
					"	<link rel=\"stylesheet\" type=\"text/css\" href=\"css/StiloviHorizontalniMeni.css\"/>\r\n"+
					"</head>\r\n" + 
					"<body>\r\n" + 
					"	<ul>\r\n" + 
					"		<li><a href=\"patient\">Početna</a></li>\r\n" + 
					"		<li><a href=\"patient\\newDose\">Zakaži novu dozu</a></li>\r\n" + 
					"		<li><a href=\"\">Odjava</a></li>\r\n" + 
					"	</ul>\r\n" + 
					"	<form method=\"post\" action=\"patient/add\">\r\n" + 
					"		<table>\r\n" + 
					"			<caption>Zakažite novu dozu</caption>\r\n" + 
					"			<tr><th>Doza:</th><td>Doza " + doseNumber + "</td></tr>\r\n" +
					"			<tr><th>Vakcina:</th>" +
					"				<td>" +
					"					<select name=\"vaccineId\">");
			for (Vaccine vaccine : vaccines.findAll()) {
				retVal.append("<option value=\"" + vaccine.getId() + "\">"
						+ vaccine.getName() 
						+ "</option>");
			}
			retVal.append("</select>" + 
					"</td>" + 
					"</tr>\r\n" + 
					"<tr><th></th><td><input type=\"submit\" value=\"Rezerviši dozu\" /></td>\r\n" + 
					"</table>\r\n" + 
					"</form>\r\n" +
					"<br/>\r\n" +
					"</body>\r\n"+
					"</html>\r\n");		
			return retVal.toString();
		}
		
		@PostMapping(value="/add")
		public void create(@RequestParam Integer vaccineId, HttpServletResponse response) throws IOException {		
			VaccineDoses vaccineDoses = (VaccineDoses) appMemory.get(UsersController.VACCINE_DOSES_KEY);
			User user = (User) appMemory.get(UsersController.USER_KEY);

			int id = 0;
			boolean notVaccinatedPrevious = false;

			for (VaccineDose dose : vaccineDoses.findAll()) {
				if(dose.getDoseId() >= id) {
					id = dose.getDoseId() + 1;
				}
				
				if(dose.getUserId() == user.getId()) {
					if(!dose.isVaccinated()) {
						notVaccinatedPrevious = true;
					}
				}
			}
			
			if(notVaccinatedPrevious) {
				System.out.println("KORISIK NIJE VAKCINISAN PRETHODNOM DOZOM");
				response.sendRedirect(bURL+"patient");

			} else {
				VaccineDose newDose = new VaccineDose(id, user.getId(), vaccineId, user.getVaccineDoses().size() + 1, null, new Date(), false); // Ispraviti ovaj dio da bude dinamičan
				newDose.setUser(user);
				vaccineDoses.vaccineDoses.put(newDose.getDoseId(), newDose);
				user.getVaccineDoses().add(newDose);
				vaccineDoses.saveInFile();
				response.sendRedirect(bURL+"patient");
			}
			
		}
		
		
		@PostMapping(value="/doseEdit")
		@ResponseBody
		public String details(@RequestParam Integer doseId) {	
			VaccineDoses vaccineDoses = (VaccineDoses) appMemory.get(UsersController.VACCINE_DOSES_KEY);
			Vaccines vaccines = (Vaccines) appMemory.get(UsersController.VACCINES_KEY);
		
			VaccineDose dose = vaccineDoses.findOne(doseId);
			
			StringBuilder retVal = new StringBuilder();
			retVal.append(
					"<!DOCTYPE html>\r\n" + 
					"<html>\r\n" + 
					"<head>\r\n" + 
					"	<meta charset=\"UTF-8\">\r\n" + 
		    		"	<base href=\""+bURL+"\">" + 
					"	<title>Knjiga</title>\r\n" + 
					"	<link rel=\"stylesheet\" type=\"text/css\" href=\"css/StiloviForma.css\"/>\r\n" + 
					"	<link rel=\"stylesheet\" type=\"text/css\" href=\"css/StiloviHorizontalniMeni.css\"/>\r\n"+
					"</head>\r\n" + 
					"<body>\r\n" + 
					"	<ul>\r\n" + 
					"		<li><a href=\"patient\">Početna</a></li>\r\n" + 
					"		<li><a href=\"patient\\newDose\">Zakaži novu dozu</a></li>\r\n" + 
					"		<li><a href=\"\">Odjava</a></li>\r\n" + 
					"	</ul>\r\n" + 				
					"	<form method=\"post\" action=\"patient/edit\">\r\n" + 
					"		<input type=\"hidden\" name=\"doseId\" value=\""+dose.getDoseId()+"\">\r\n" + 
					"		<table>\r\n" + 
					"			<caption>Doza: "+ dose.getDoseId() +"</caption>\r\n" +
				 	"			<tr><th>ID doze:</th><td>" + dose.getDoseId() + "</td></tr>\r\n" + 
	 				"			<tr><th>Broj doze:</th><td>" + dose.getNumberOfDose() + "</td></tr>\r\n" + 
	 				"			<tr><th>Vakcina:</th><td>" +
					"					<select name=\"vaccineId\">"); 
			
					retVal.append("<option value=\"" + dose.getVaccineId() + "\">"
							+ vaccines.findOne(dose.getVaccineId()).getName() 
							+ "</option>");
					
						for (Vaccine vaccine : vaccines.findAll()) {
							if(vaccine.getId() != dose.getVaccineId()) {
								retVal.append("<option value=\"" + vaccine.getId() + "\" name=\"vaccineId\">"
										+ vaccine.getName() 
										+ "</option>");
							}
						}
						
					retVal.append("</select>" +
					"			</td></tr>\r\n" +
					"			<tr><th></th><td><input type=\"submit\" value=\"Izmjeni\" /></td>\r\n" + 
					"		</table>\r\n" + 
					"	</form>\r\n" + 
					"	<br/>\r\n" + 
					"	<form method=\"post\" action=\"patient/delete\">\r\n" + 
					"		<input type=\"hidden\" name=\"id\" value=\""+dose.getDoseId()+"\">\r\n" + 
					"		<table>\r\n" + 
					"			<tr><th></th><td><input type=\"submit\" value=\"Otkaži dozu\"></td>\r\n" + 
					"		</table>\r\n" + 
					"	</form>\r\n" +
					"	<br/>\r\n" + 
					"	<br/>\r\n"); 
				
			
			retVal.append(
					"</body>\r\n"+
					"</html>\r\n");		
			return retVal.toString();
		}
	
		
		@PostMapping(value="/edit")
		public void Edit(@ModelAttribute VaccineDose vaccineDoseEdit , HttpServletResponse response) throws IOException {	

			VaccineDoses vaccineDoses = (VaccineDoses) appMemory.get(UsersController.VACCINE_DOSES_KEY);
			
			VaccineDose dose = vaccineDoses.findOne(vaccineDoseEdit.getDoseId());
			if(dose != null) {
					dose.setVaccineId(vaccineDoseEdit.getVaccineId());
			}
			vaccineDoses.saveInFile();
			response.sendRedirect(bURL+"patient");
		}	
		
		@PostMapping(value="/delete")
		public void delete(@RequestParam Integer id, HttpServletResponse response) throws IOException {

			VaccineDoses vaccineDoses = (VaccineDoses) appMemory.get(UsersController.VACCINE_DOSES_KEY);
			User user = (User) appMemory.get(UsersController.USER_KEY);
			/*
			for (VaccineDose userDose : user.getVaccineDoses()) {
				if(userDose.getDoseId() == id) {
					user.vaccineDoses.remove(2);
					System.out.println("IZBRISANA JE DOZA");
				}
			}
			
			*/
			
			VaccineDose deleted = vaccineDoses.delete(id);
			vaccineDoses.saveInFile();
			response.sendRedirect(bURL+"patient");
		}
	
}
