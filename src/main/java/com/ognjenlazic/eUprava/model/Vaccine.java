package com.ognjenlazic.eUprava.model;

public class Vaccine {
	
	public Vaccine(int id, String name) {
		super();
		this.id = id;
		this.name = name;
	}
	
	private String name;
	private int id;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
}
