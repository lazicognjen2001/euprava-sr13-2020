package com.ognjenlazic.eUprava.model;

import java.util.ArrayList;
import java.util.List;

public class User {
	
	public User(int id, long jmbg, String name, String surname, UserType userType, String password) {
		this.id = id;
		this.jmbg = jmbg;
		this.name = name;
		this.surname = surname;
		this.password = password;
		this.userType = userType;
	}
	
	public User() {
		
	}

	private int id;
	private long jmbg;
	private String name;
	private String surname;
	private UserType userType; 
	public List<VaccineDose> vaccineDoses = new ArrayList<VaccineDose>();
	private String password;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	
	public List<VaccineDose> getVaccineDoses() {
		return vaccineDoses;
	}
	public void setVaccineDoses(List<VaccineDose> vaccineDoses) {
		this.vaccineDoses = vaccineDoses;
	}
	
	public Long getJmbg() {
		return jmbg;
	}
	public void setJmbg(Long jmbg) {
		this.jmbg = jmbg;
	}
	
	public UserType getUserType() {
		return userType;
	}
	public void setUserType(UserType userType) {
		this.userType = userType;
	}
	
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
}
