package com.ognjenlazic.eUprava.model;

import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Vaccines {
private Map<Integer, Vaccine> vaccines = new HashMap<>();
	
	public Vaccines() {
		
		try {
			Path path = Paths.get(getClass().getClassLoader().getResource("vaccine.txt").toURI());
			System.out.println(path.toFile().getAbsolutePath());
			List<String> lines = Files.readAllLines(path, Charset.forName("UTF-8"));

			for (String line : lines) {
				line = line.trim();
				
				if (line.equals("") || line.indexOf('#') == 0)
					continue;
				
				
				String[] tokens = line.split("\\|");
				
				int id = Integer.parseInt(tokens[0]);
				String name = tokens[1];

				vaccines.put(id, new Vaccine(id, name));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public List<Vaccine> findAll() {
		return new ArrayList<Vaccine>(vaccines.values());
	}
	
	public Vaccine findOne(Integer id) {
		Vaccine vaccineFind = null;
		for (Vaccine vaccine: vaccines.values()) {
			if(vaccine.getId() == id) {
				vaccineFind = vaccine;
			}
		}
		return vaccineFind;
	}
}
