package com.ognjenlazic.eUprava.model;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;




public class VaccineDoses {
	 public Map<Integer, VaccineDose> vaccineDoses = new HashMap<>();
	
	public VaccineDoses() {
		
		try {
			Path path = Paths.get(getClass().getClassLoader().getResource("vaccineDose.txt").toURI());
			System.out.println(path.toFile().getAbsolutePath());
			List<String> lines = Files.readAllLines(path, Charset.forName("UTF-8"));

			for (String line : lines) {
				line = line.trim();
				
				if (line.equals("") || line.indexOf('#') == 0)
					continue;
				
			    SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm");  
				
				String[] tokens = line.split("\\|");
				
				int id = Integer.parseInt(tokens[0]);
				int userId = Integer.parseInt(tokens[1]);
				int vaccineId = Integer.parseInt(tokens[2]);
				int numberOfDose = Integer.parseInt(tokens[3]);
				
				Date vaccinationDate;
				if (tokens[4].equals("0")) {
					vaccinationDate = null;
				} else {
					vaccinationDate = formatter.parse(tokens[4]);
				}
				
				Date reservationDate  = formatter.parse(tokens[5]);
				
				boolean isVaccinated;
				if (tokens[6].equals("true")){
					isVaccinated = true;
				} else {
					isVaccinated = false;
				}

				vaccineDoses.put(id, new VaccineDose(id, userId, vaccineId, numberOfDose, vaccinationDate, reservationDate, isVaccinated));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public List<VaccineDose> findAll() {
		return new ArrayList<VaccineDose>(vaccineDoses.values());
	}
	
	public VaccineDose vaccinate(int id) {
		if (!vaccineDoses.containsKey(id)) {
			throw new IllegalArgumentException("tried to vaccinate non existing dose");
		}
		VaccineDose vaccineDose = vaccineDoses.get(id);
		if (vaccineDose != null) {
			vaccineDose.setVaccinated(true);
			vaccineDose.setDateOfDose(new Date());
		}
		return vaccineDose;
	}
	
	public VaccineDose findOne(Integer id) {
		VaccineDose dose = null;
		for (VaccineDose doses: vaccineDoses.values()) {
			if(doses.getDoseId() == id) {
				dose = doses;
			}
		}
		return dose;
	}
	
	public VaccineDose delete(Integer id) {
		if (!vaccineDoses.containsKey(id)) {
			throw new IllegalArgumentException("tried to remove non existing book");
		}
		VaccineDose dose = vaccineDoses.get(id);
		if (dose != null) {
			vaccineDoses.remove(id);
		}
		return dose;
	}
	
	public void saveInFile() {
		
		try {
			Path path = Paths.get(getClass().getClassLoader().getResource("vaccineDose.txt").toURI());
			System.out.println(path.toFile().getAbsolutePath());
			List<String> lines = Files.readAllLines(path, Charset.forName("UTF-8"));
	        File fajl = new File(path.toString());

			PrintWriter writer = new PrintWriter(fajl);
			writer.print("");
			writer.close();
			
			

			for (VaccineDose dose : vaccineDoses.values()) {
				
				BufferedWriter bw = null;
			    try {
			        
			        if (!fajl.exists()) {
			            fajl.createNewFile();
			        }

			        FileWriter fw = new FileWriter(fajl, true);
			        bw = new BufferedWriter(fw);
			        
			        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm"); 
			        String dateOfDose = "0";  

			        if (dose.getDateOfDose() != null) {
				        dateOfDose = dateFormat.format(dose.getDateOfDose());  
			        }
			        String reservationDate = dateFormat.format(dose.getDateOfReservation()); 
			        
			        bw.write(dose.getDoseId() + "|" + dose.getUserId() + "|" + dose.getVaccineId() + "|" + dose.getNumberOfDose() + "|" + dateOfDose + "|" + reservationDate + "|" + dose.isVaccinated() + "\n");
			        System.out.println(dose.getDoseId() + "|" + dose.getUserId() + "|" + dose.getVaccineId() + "|" + dose.getNumberOfDose() + "|" + dateOfDose + "|" + reservationDate + "|" + dose.isVaccinated());

			        
			        System.out.println("PONUDE UPISANE U FAJL");
			    } catch (IOException e) {
			        System.out.println("GRESKA!" + e);
			    }
			    finally {
			        try{
			            if(bw!=null)
			                bw.close();
			        }catch(Exception ex){
			            System.out.println("Error in closing the BufferedWriter"+ex);
			        }
			    }
				
				
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
}


/* Test data:
200|1|100|1|01-09-2021 08:00|14-07-2021 11:31|true
201|1|100|2|12-12-2021 08:00|10-11-2021 11:32|true
202|2|101|1|0|10-11-2021 11:32|false
203|3|102|1|10-12-2021 08:00|10-11-2021 13:31|true
*/