package com.ognjenlazic.eUprava.model;

import java.util.Date;

public class VaccineDose {
	
	// .txt file form:
	// doseId|userId|vaccineId|numberOfDose|dateOfReservation|dateOfDose|isVaccinated
	
	public VaccineDose() {}
	
	public VaccineDose(int doseId, int userId, int vaccineId, int numberOfDose, Date vaccinationDate, Date reservationDate, boolean isVaccinated) {
		super();
		this.doseId = doseId;
		this.userId = userId;
		this.numberOfDose = numberOfDose;
		this.vaccineId = vaccineId;
		this.dateOfDose = vaccinationDate;
		this.dateOfReservation = reservationDate;
		this.isVaccinated = isVaccinated;
	}
	
	private int doseId;
	private int numberOfDose;
	private Vaccine vaccine;
	private User user;
	private int userId;
	private int vaccineId;
	private Date dateOfDose;
	private Date dateOfReservation;
	private boolean isVaccinated;
	
	public Vaccine getVaccine() {
		return vaccine;
	}
	public void setVaccine(Vaccine vaccine) {
		this.vaccine = vaccine;
	}
	
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	
	public int getDoseId() {
		return doseId;
	}
	public void setDoseId(int doseId) {
		this.doseId = doseId;
	}
	
	public int getVaccineId() {
		return vaccineId;
	}
	public void setVaccineId(int vaccineId) {
		this.vaccineId = vaccineId;
	}
	
	public int getUserId() {
		return userId;
	}
	
	public void setUserId(int userId) {
		this.userId = userId;
	}
	
	public Date getDateOfDose() {
		return dateOfDose;
	}
	public void setDateOfDose(Date dateOfDose) {
		this.dateOfDose = dateOfDose;
	}
	
	public int getNumberOfDose() {
		return numberOfDose;
	}
	public void setNumberOfDose(int numberOfDose) {
		this.numberOfDose = numberOfDose;
	}
	
	public boolean isVaccinated() {
		return isVaccinated;
	}
	public void setVaccinated(boolean isVaccinated) {
		this.isVaccinated = isVaccinated;
	}
	
	public Date getDateOfReservation() {
		return dateOfReservation;
	}
	public void setDateOfReservation(Date dateOfReservation) {
		this.dateOfReservation = dateOfReservation;
	}
	
}
