package com.ognjenlazic.eUprava.model;

import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class Users {
	
	public Map<Integer, User> users = new HashMap<>();
	
	public Users() {
		try {
			System.out.println("ČITAMO KORISNIKE");
			Path path = Paths.get(getClass().getClassLoader().getResource("users.txt").toURI());
			System.out.println(path.toFile().getAbsolutePath());
			List<String> lines = Files.readAllLines(path, Charset.forName("UTF-8"));
			System.out.println("Ne znam ");

			for (String line : lines) {
				System.out.println(line);

				line = line.trim();
				
				if (line.equals("") || line.indexOf('#') == 0)
					continue;

				String[] tokens = line.split("\\|");
				System.out.println(line);
				int id = Integer.parseInt(tokens[0]);
				long jmbg = Long.parseLong(tokens[1]);
				String name = tokens[3];
				String surname = tokens[4];
				String password = tokens[2];
				
				UserType userType;
				if (tokens[5].equals("patient")) {
					userType = UserType.patient;
				} else if (tokens[5].equals("medic")) {
					userType = UserType.medic;
				} else {
					userType = UserType.medic;
				}
				System.out.println(userType.toString());
				users.put(id, new User(id, jmbg, name, surname, userType, password));
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public User findOneByJmbg(long jmbg) {
		User found = null;
		for (User user : users.values()) {
			if (user.getJmbg() == jmbg) {
				found = user;
				break;
			}
		}
		return found;
	}
	
	public User findOne(int id) {
		return users.get(id);
	}
	
	public List<User> findAll() {
		return new ArrayList<User>(users.values());
	}
}
